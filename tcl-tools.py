#!/usr/bin/env python3

import requests
import sys
from itertools import cycle
from flask import Flask, request
from flask_restful import Resource, Api
from flask_jsonpify import jsonify
import time
import os


modes = ["darker", "dark", "normal", "bright", "brighter"]
app = Flask(__name__)
api = Api(app)
url = "http://"+sys.argv[1]+":8060/keypress/"

def press_key(key):
    requests.post(url=url+key, data="")

class brightness(Resource):
    def get(self, new_brightness):
        new_brightness = new_brightness.lower()
        if new_brightness not in modes:
            return jsonify({'error': True})
        current_brightness = ""
        with open('brightness.txt', 'r') as file:
            current_brightness = file.read().replace('\n', '')
        current_index = modes.index(current_brightness)
        new_index = modes.index(new_brightness)
        scroll = calculate_movement(current_index, new_index)
        keypress = ""
        keypress = "Left" if scroll < 0 else "Right"
        press_key("Info")
        count = 0
        while count < abs(scroll):
            press_key(keypress)
            count = count + 1
        press_key("Info")
        with open("brightness.txt", 'w') as file:
            file.write(new_brightness)
        return jsonify({'brightness': new_brightness})

def calculate_movement(current_index, new_index):
    current_index = current_index + 1
    new_index = new_index + 1
    left_scroll = -((current_index - new_index) % 5)
    right_scroll = (new_index - current_index) % 5
    return right_scroll if abs(right_scroll) < abs(left_scroll) else left_scroll

class toggle_gamemode(Resource):
    def get(self, command):
        status_file = "gamemode.txt"
        state = ""
        with open(status_file, "r") as file:
            state = file.read().replace('\n', '')
        if(state == "on" and command == "on") or (state == "off" and command == "off"):
            return
        press_key("Info")
        press_key("Up")
        press_key("Right")
        press_key("Up")
        press_key("Up")
        time.sleep(0.1)
        press_key("Right")
        press_key("Info")
        with open(status_file, "w") as file:
            if(state=="on"):
                file.write("off")
            else:
                file.write("on")
        return jsonify({'game_mode': command})



class current_state(Resource):
    def get(self):
        game_mode = ""
        brightness = ""
        with open('brightness.txt', 'r') as current_brightness, open('gamemode.txt', 'r') as current_gamemode:
            game_mode = current_gamemode.read().replace('\n', '')
            brightness = current_brightness.read().replace('\n', '')
        state = {'brightness': brightness, 'game_mode': game_mode}
        return jsonify(state)


api.add_resource(current_state, '/state')
api.add_resource(brightness, '/brightness/<new_brightness>')
api.add_resource(toggle_gamemode, '/gamemode/<command>')

if __name__ == "__main__":
    with open("./gamemode.txt", "r") as gamemode, open("./brightness.txt") as brightness:
        if(gamemode.read().replace('\n', '') not in ["off", "on"]) or (brightness.read().replace('\n', '') not in modes):
           print("Please populate gamemode.txt with <on, off> and brightness.txt with <darker, dark, normal, bright, brighter>")
           exit(1) 
    app.run(host="0.0.0.0",port=5002)